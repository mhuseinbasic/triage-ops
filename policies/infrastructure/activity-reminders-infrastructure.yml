.common_conditions:
  group_exemptions: &group_exemptions
    assignee_member:
      source: group
      condition: member_of
      source_id: gitlab-com/gl-infra

resource_rules:
  issues:
    rules:
      - name: Remind stale infrastructure issues
        conditions:
          <<: *group_exemptions
          date:
            attribute: updated_at
            condition: older_than
            interval_type: months
            interval: 4
          state: opened
          limit:
            most_recent: 10
        actions:
          labels:
            - auto updated
          comment: |
            Hi {{author}},

            First of all, thank you for raising this issue.

            We're posting this message because this issue meets the following criteria:

            * This issue is open
            * No activity in the past 4 months (since {{updated_at}})

            We'd like to ask you to help us out and determine how we should act on this issue.

            If this issue is proposing an infrastructure change or reporting an issue can you verify
            whether it is still relevant?

            Thanks for your help! :black_heart:

            ----

            You are welcome to help [improve this comment](https://gitlab.com/gitlab-com/gl-infra/triage-ops/blob/master/activity-reminders-infrastructure.yml).

      - name: Remind old infrastructure issues without any labels
        conditions:
          <<: *group_exemptions
          date:
            attribute: created_at
            condition: older_than
            interval_type: months
            interval: 1
          labels:
            - No Label
          state: opened
          forbidden_labels:
            - auto updated
        limit:
          most_recent: 10
        actions:
          labels:
            - auto updated
          comment: |
            Hi {{author}},

            First of all, thank you for raising this issue.

            We're posting this message because this issue meets the following criteria:

            * This issue is open
            * Was opened a month ago (on {{updated_at}})
            * It does not have any labels

            We'd like to ask you to help us out and determine how we should act on this issue.
            For issues that have been sitting in the backlog for a long period of time
            it is helpful for scheduling to assign a severity. Please assign a severity label given
            what is currently understood about this issue.

            Thanks for your help! :black_heart:

            ----

            You are welcome to help [improve this comment](https://gitlab.com/gitlab-com/gl-infra/triage-ops/blob/master/policies/infrastructure/activity-reminders-infrastructure.yml).
