# GitLab triage operations

This repository is for adding triage labels to infrastucture projects
## The schedules

### Triage operations schedules (hourly)

Every hour [a pipeline schedule](https://gitlab.com/gitlab-com/gl-infra/triage-ops/pipeline_schedules)
will run which will process the rules for the production and infrastructure issue queues.

There are two policy directories, one for each issue queue:

* [infrastructure policies](https://gitlab.com/gitlab-com/gl-infra/triage-ops/tree/master/policies/infrastructure)
* [production policies](https://gitlab.com/gitlab-com/gl-infra/triage-ops/tree/master/policies/production)

After an issue has been updated the `~auto updated` label is added preventing
further comments, except for the checks that look for inactivity. This prevents
multiple reviews from updating the same issue (the first rule will win). We may
want to fix this by removing the label after a window of time or by using
different labels for different rules.

### Rules

The following rules are implemented:

### Production issues

* Reminder to update all issues that have not seen any activity after 3 weeks
* Reminder to update incident issues that do not have any labels
* Reminder to add service attribution to incident issues
* Reminder to add severity on change and incident issues

### Infrastructure issues

* Reminder to update issues that are assigned to someone in the team and haven't been updated for 4 months.
* Reminder to update issues that are assigned to someone in the team that are older than 1 month that have no labels
* Reminder to update issues that are assigned to someone in the team that do not have a weight assigned.

## The bot

We're using [@gitlab-bot](https://gitlab.com/gitlab-bot) as the user to run
triage operations. The credentials could be found in the shared 1Password
vault.

This bot is a shared user that is also used for the gitlab-ce and gitlab-ee
repository checks. For more information see https://gitlab.com/gitlab-org/quality/triage-ops
